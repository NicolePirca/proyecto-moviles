package com.example.proyectomoviles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore

class NuevoCliente : AppCompatActivity() {
    private lateinit var registroButton: Button
    private lateinit var backButton: Button
    private lateinit var nombretxt: EditText
    private lateinit var correotxt: EditText
    val db = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nuevo_cliente)

        registroButton = findViewById(R.id.btnGuardarNuevoCliente)
        backButton = findViewById(R.id.btnReturnNuevoCliente)
        nombretxt = findViewById(R.id.txtNombreNuevoCliente)
        correotxt = findViewById(R.id.txtNombreEmpleado)
        registroButton.setOnClickListener{registrar()}
        backButton.setOnClickListener{back()}
    }

    private fun back(){
        finish()
    }

    private  fun registrar(){
        registroButton.isEnabled=false
        val nombre = nombretxt.text.toString()
        val email = correotxt.text.toString()

        if(email.isNullOrEmpty() || nombre.isNullOrEmpty()){
            Toast.makeText(baseContext, "Campos vacíos",
                Toast.LENGTH_SHORT).show()
        }else{


                        val usuario = hashMapOf(
                            "nombre" to nombre,
                            "correo" to email

                        )
                        db.collection("users").add(usuario as Map<String, Any>)
                            .addOnSuccessListener { documentReference ->
                                Toast.makeText(baseContext, "Cliente Guardado Existosamente.",
                                    Toast.LENGTH_SHORT).show()
                                registroButton.isEnabled=true
                                finish()
                            }
                            .addOnFailureListener { e ->
                                Toast.makeText(baseContext, "Error al Guardar Cliente.",
                                    Toast.LENGTH_SHORT).show()
                                registroButton.isEnabled=true
                            }



                    // ...

        }


    }
}
