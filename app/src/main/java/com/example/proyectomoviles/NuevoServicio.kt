package com.example.proyectomoviles

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.firestore.FirebaseFirestore

class NuevoServicio : AppCompatActivity() {


        private lateinit var serviceButton: Button
        private lateinit var backButton: Button

        private lateinit var nombreService: EditText
        private lateinit var valorService: EditText
        val db = FirebaseFirestore.getInstance()

        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            setContentView(R.layout.activity_nuevo_servicio)

            serviceButton = findViewById(R.id.btnGuardarNuevoServicio)
            backButton = findViewById(R.id.btnReturnNuevoServicio)

            nombreService = findViewById(R.id.txtNombreEmpleado)
            valorService = findViewById(R.id.txtTelefonoEmpleado)
            serviceButton.setOnClickListener { registrar() }
            backButton.setOnClickListener { back() }
        }

    private fun back(){
        finish()
    }

    private  fun registrar(){
        serviceButton.isEnabled=false

        val nombre = nombreService.text.toString()
        val valor = valorService.text.toString().toLong()

        if(nombre.isNullOrEmpty()){

            Toast.makeText(baseContext, "Campos vacíos",
                Toast.LENGTH_SHORT).show()
        }else{


            val usuario = hashMapOf(

                "nombre" to nombre,
                "valor" to valor
            )
            db.collection("services").add(usuario as Map<String, Any>)
                .addOnSuccessListener { documentReference ->
                    Toast.makeText(baseContext, "Servicio Guardado Existosamente.",
                        Toast.LENGTH_SHORT).show()
                    serviceButton.isEnabled=true
                    finish()
                }
                .addOnFailureListener { e ->
                    Toast.makeText(baseContext, "Error al Guardar el Servicio.",
                        Toast.LENGTH_SHORT).show()
                    serviceButton.isEnabled=true
                }



            // ...

        }


    }

    }

