package com.example.proyectomoviles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class LoginActivity : AppCompatActivity() {

    private lateinit var loginButton: Button
    private lateinit var registroButton: Button
    private lateinit var correotxt: EditText
    private lateinit var passwordtxt: EditText
    private lateinit var auth: FirebaseAuth


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        auth = FirebaseAuth.getInstance()
        loginButton = findViewById(R.id.btnLogin)
        registroButton = findViewById(R.id.btnRegistrarseLogin)
        correotxt = findViewById(R.id.txtEmailLogin)
        passwordtxt = findViewById(R.id.txtPasswordLogin)
        loginButton.setOnClickListener{iniciarSesion()}
        registroButton.setOnClickListener{registrar()}
        val user = auth.currentUser
        if(user != null){
            saltarLogin()
        }
    }

    private fun saltarLogin(){
        val mainActivityIntent = Intent (this, MainActivity::class.java)
        startActivity(mainActivityIntent)
    }

    private fun iniciarSesion(){
        loginButton.isEnabled=false
        var email = correotxt.text.toString()
        var password = passwordtxt.text.toString()
        if(email.isNullOrEmpty() || password.isNullOrEmpty()){
            Toast.makeText(baseContext, "Campos vacíos",
                Toast.LENGTH_SHORT).show()
        }else{
            auth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information
                        val user = auth.currentUser
                        Toast.makeText(baseContext, "Login Exitoso.",
                            Toast.LENGTH_SHORT).show()
                        val mainActivityIntent = Intent (this, MainActivity::class.java)
                        startActivity(mainActivityIntent)

                    } else {
                        // If sign in fails, display a message to the user.
                        Toast.makeText(baseContext, "Error Login.",
                            Toast.LENGTH_SHORT).show()

                    }
                    loginButton.isEnabled=true

                    // ...
                }
        }


    }

    private fun registrar(){
        val registerActivityIntent = Intent (this, RegisterActivity::class.java)

        startActivity(registerActivityIntent)

    }
}
