package com.example.proyectomoviles.models

class Servicio{
    var idServicio:String=""
    var nombre:String=""
    var valor:Long=0
    var empleados:ArrayList<String> = ArrayList<String>()

    constructor(data:Map<String,Any>, id:String){
        idServicio=id
        nombre=data["nombre"].toString()
        valor= data["valor"] as Long
        if(data["empleados"] != null){

            empleados = data["empleados"] as ArrayList<String>
        }
    }
}