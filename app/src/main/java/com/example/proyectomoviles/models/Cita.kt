package com.example.proyectomoviles.models

import java.util.*

class Cita {
    var fecha: String = ""
    var idUsuario: String = ""
    var idEmpleado: String = ""
    var idServicio: String = ""
    var idCita: String = ""
    var horario: String = ""

    constructor(data:Map<String,Any>, id:String){
        idCita=id
        fecha=data["fecha"].toString()
        idUsuario= data["idUsuario"] .toString()
        idEmpleado = data["idEmpleado"] .toString()
        idServicio= data["idServicio"].toString()
        horario= data["horario"].toString()

    }
}