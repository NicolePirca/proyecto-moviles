package com.example.proyectomoviles

import android.content.ContentValues
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import com.example.proyectomoviles.models.Servicio
import com.google.firebase.firestore.FirebaseFirestore

class NuevoEmpleado : AppCompatActivity() {

    private lateinit var empleadoButton: Button
    private lateinit var backButton: Button

    private lateinit var nombreEmpleado: EditText
    private lateinit var telefonoEmpleado: EditText
    lateinit var spinnerServicio1: Spinner
    lateinit var servicioSeleccionado: Servicio
    var servicesListModel = ArrayList<Servicio>()
    var servicesList = ArrayList<String>()
    val db = FirebaseFirestore.getInstance()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_nuevo_empleado)

        empleadoButton = findViewById(R.id.btnGuardarNuevoEmpleado)
        backButton = findViewById(R.id.btnReturnNuevoEmpleado)
        spinnerServicio1 = findViewById(R.id.spinnerServiciosEmpleado)
        nombreEmpleado = findViewById(R.id.txtNombreEmpleado)
        telefonoEmpleado = findViewById(R.id.txtTelefonoEmpleado)

        empleadoButton.setOnClickListener { registrar() }
        backButton.setOnClickListener { back() }

        cargarServicios()
        addServiceSpinnerListener()

    }

    private fun back(){
        finish()
    }

    private  fun registrar(){
        empleadoButton.isEnabled=false

        val nombre = nombreEmpleado.text.toString()
        val telefono = telefonoEmpleado.text.toString()

        if(nombre.isNullOrEmpty()){

            Toast.makeText(baseContext, "Campos vacíos",
                Toast.LENGTH_SHORT).show()
        }else{


            val usuario = hashMapOf(

                "nombre" to nombre,
                "telefono" to telefono
            )
            var idEmpleado = ""
            db.collection("employees").add(usuario as Map<String, Any>)
                .addOnSuccessListener { documentReference ->
                    Toast.makeText(baseContext, "Empleado Guardado Existosamente.",
                        Toast.LENGTH_SHORT).show()
                    empleadoButton.isEnabled=true
                    idEmpleado = documentReference.id
                    var empleados = servicioSeleccionado.empleados
                    empleados.add(idEmpleado)
                    Log.d("empleadosServ", empleados.toString())
                    val data:MutableMap<String,Any> = hashMapOf(
                        "empleados" to empleados
                    )
                    db.collection("services").document(servicioSeleccionado.idServicio).update(data)
                        .addOnSuccessListener { documentReference ->
                            Log.d("ActualizacionServicio", "Servicio Actualizado Satisfactoriamente")
                        }
                        .addOnFailureListener { e ->
                            Log.w("ActualizacionServicio", "Error Actualizando Servicio", e)
                        }

                    finish()
                }
                .addOnFailureListener { e ->
                    Toast.makeText(baseContext, "Error al Guardar el Empleado.",
                        Toast.LENGTH_SHORT).show()
                    empleadoButton.isEnabled=true
                }



            // ...

        }


    }

    fun cargarServicios() {

        var adapterServicio =  ArrayAdapter<String>(
            this,
            android.R.layout.simple_spinner_dropdown_item,
            servicesList
        )
        adapterServicio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerServicio1.adapter=adapterServicio

        db.collection("services")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d("servicios", "${document.id} => ${document.data}")
                    var servicio = Servicio(document.data,document.id)
                    servicesList.add(servicio.nombre)
                    servicesListModel.add(servicio)


                }
                adapterServicio.notifyDataSetChanged()
                //println(employeesList)
            }
            .addOnFailureListener { exception ->
                Log.w(ContentValues.TAG, "Error getting documents.", exception)
            }
    }

    fun addServiceSpinnerListener(){
        spinnerServicio1.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                 servicioSeleccionado = servicesListModel[position]

            }
        }
    }
}
