package com.example.proyectomoviles.ui.citas

import android.content.ContentValues.TAG
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.proyectomoviles.R
import com.example.proyectomoviles.models.Cita
import com.example.proyectomoviles.models.Servicio
import com.example.proyectomoviles.ui.empleados.FragmentEmpleado
import kotlinx.android.synthetic.main.fragment_citas_fragment.*
import java.util.*
import kotlin.collections.ArrayList
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter


class FragmentCitas : Fragment() {
    companion object {
        fun newInstance() = FragmentCitas()
    }


    private lateinit var viewModel: FragmentCitasViewModel
    val db = FirebaseFirestore.getInstance()
    lateinit var spinnerServicio1: Spinner
    lateinit var spinnerHorario: Spinner
    var employeesList = mutableListOf<String>()
    var availableEmployees = mutableListOf<String>()
    var servicesList = ArrayList<String>()
    var servicesListModel = ArrayList<Servicio>()
    var strHorarios = arrayOf("9-10", "10-11", "11-12", "12-13")
    var empleadoSeleccionado: String = ""
    private lateinit var empleadoAsignado: TextView
    private lateinit var calendario: CalendarView
    private lateinit var btnAgendar: Button
    private lateinit var fechaCita: String
    private lateinit var horarioSeleccionado: String
    private lateinit var idServicioSeleccionado: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_citas_fragment, container, false)
        spinnerServicio1 = view.findViewById(R.id.spinnerServicio)
        spinnerHorario = view.findViewById(R.id.spinnerHorario)
        calendario = view.findViewById(R.id.calendarView)
        empleadoAsignado = view.findViewById(R.id.txtEmpleadoAsignado)
        btnAgendar =view.findViewById(R.id.buttonCita)

        btnAgendar.setOnClickListener{crearCita()}
        cargarServicios()
        cargarHorarios()
        addHorarioSpinnerListener()
        addServiceSpinnerListener()
        return view
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FragmentCitasViewModel::class.java)

    }

    fun cargarHorarios() {

        val adapter = ArrayAdapter<String>(
            context!!,
            android.R.layout.simple_spinner_dropdown_item,
            strHorarios
        )
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerHorario.adapter = adapter
    }

    fun crearCita() {


        val idUsuario = FirebaseAuth.getInstance().currentUser!!.uid
        val citaHash = hashMapOf(
            "fecha" to fechaCita,
            "horario" to horarioSeleccionado,
            "idEmpleado" to empleadoSeleccionado,
            "idServicio" to idServicioSeleccionado,
            "idUsuario" to idUsuario

        )
        db.collection("citas").add(citaHash as Map<String, Any>)
            .addOnSuccessListener { documentReference ->
                Toast.makeText(
                    context, "Cita Registrada",
                    Toast.LENGTH_SHORT
                ).show()
                //Intent de exito
            }
            .addOnFailureListener { e ->
                Toast.makeText(
                    context, "Registro Fallido en Firestore.",
                    Toast.LENGTH_SHORT
                ).show()
                //Advice
            }
    }


    fun addServiceSpinnerListener() {
        spinnerServicio1.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                val servicioSeleccionado: Servicio = servicesListModel[position]
                idServicioSeleccionado = servicioSeleccionado.idServicio
                employeesList = servicioSeleccionado.empleados
                Log.d(
                    "empleados",
                    "${servicioSeleccionado.idServicio} => ${servicioSeleccionado.empleados}"
                )
            }
        }
    }

    fun addHorarioSpinnerListener() {

        spinnerHorario.onItemSelectedListener = object :
            AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                horarioSeleccionado = strHorarios[position]
                Log.d("horarios", " ${horarioSeleccionado}")

                obtenerCitasFiltradas(horarioSeleccionado)
                availableEmployees = employeesList
                asignarEmpleado()
            }
        }
    }

    fun obtenerCitasFiltradas(horario: String) {
        val fecha = Date(calendario.date)
        val fechaArray = fecha.toString().split(" ")
        val dateString = "" + fechaArray[5] + "-" + fechaArray[1] + "-" + fechaArray[2]
        fechaCita = dateString
        Log.d("horarios", " ${dateString}")
        val citasReference = db.collection("citas").whereEqualTo("horario", horario)
            .whereEqualTo("fecha", dateString)
        citasReference.get().addOnSuccessListener { result ->
            for (document in result) {
                Log.d("citas", "${document.id} => ${document.data}")
                var cita = Cita(document.data, document.id)
                descartarEmpleadosOcupados(cita)

            }

            Log.d("empleados Disponibles", "${employeesList}")
            //adapterServicio.notifyDataSetChanged()
            //println(employeesList)
        }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }

    }

    fun descartarEmpleadosOcupados(cita: Cita) {
        val idEmpleadoCita = cita.idEmpleado
        availableEmployees.remove(idEmpleadoCita)

    }

    fun cargarServicios() {

        var adapterServicio = ArrayAdapter<String>(
            context!!,
            android.R.layout.simple_spinner_dropdown_item,
            servicesList
        )
        adapterServicio.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        spinnerServicio1.adapter = adapterServicio

        db.collection("services")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    Log.d("servicios", "${document.id} => ${document.data}")
                    var servicio = Servicio(document.data, document.id)
                    servicesList.add(servicio.nombre)
                    servicesListModel.add(servicio)


                }
                adapterServicio.notifyDataSetChanged()
                //println(employeesList)
            }
            .addOnFailureListener { exception ->
                Log.w(TAG, "Error getting documents.", exception)
            }
    }

    fun asignarEmpleado() {
        if (availableEmployees.count() > 0) {
            availableEmployees.shuffle()

            empleadoSeleccionado = availableEmployees[0]
            buscarNombreEmpleado(empleadoSeleccionado)
        } else {
            empleadoAsignado.text = "No hay empleados disponibles"
        }
    }

    fun buscarNombreEmpleado(id: String) {
        Log.d("empleados", id)
        val nombreEmpleado = db.collection("employees").document(id)
        nombreEmpleado.get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    empleadoAsignado.text = document.getString("nombre")
                    Log.d("empleados", id)
                } else {
                    Log.d("error", "No existe documento")
                }
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "get failed with ", exception)
            }
    }



}
