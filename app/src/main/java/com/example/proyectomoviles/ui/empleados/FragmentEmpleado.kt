package com.example.proyectomoviles.ui.empleados

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import com.example.proyectomoviles.NuevoEmpleado
import com.example.proyectomoviles.R
import com.example.proyectomoviles.models.Empleado
import com.example.proyectomoviles.models.Usuario
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore


class FragmentEmpleado : Fragment() {

    companion object {
        fun newInstance() = FragmentEmpleado()
    }

    private lateinit var viewModel: FragmentEmpleadoViewModel
    private lateinit var listView:ListView
    private lateinit var nuevoEmpleadoButton:FloatingActionButton
    private val listaEmpleados = arrayListOf<Empleado>()
    val db = FirebaseFirestore.getInstance()
    var listItems = ArrayList<String>()
    var adapter:ArrayAdapter<String>? = null


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewFragmentEmpleado = inflater.inflate(R.layout.fragment_empleado_fragment,container, false)
        listView = viewFragmentEmpleado.findViewById(R.id.listEmpleado)
        adapter = ArrayAdapter(activity!!.baseContext, android.R.layout.simple_list_item_1, listItems)
        listView.adapter = adapter
        nuevoEmpleadoButton = viewFragmentEmpleado.findViewById(R.id.nuevoEmpleadoButton)
        nuevoEmpleadoButton.setOnClickListener{ openNuevoEmpleadoView() }
        leerFirestore()
        return viewFragmentEmpleado
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FragmentEmpleadoViewModel::class.java)
        // TODO: Use the ViewModel
    }

    fun openNuevoEmpleadoView(){
        val newClienteActivityIntent = Intent (activity, NuevoEmpleado::class.java)
        startActivity(newClienteActivityIntent)

    }

    private fun leerFirestore(){
        db.collection("employees")
            .get()
            .addOnSuccessListener { result ->

                listItems.add("NOMBRE: TELEFONO")
                for (document in result) {
                    val empleado: Empleado = Empleado(document.data)
                    listaEmpleados.add(empleado)
                    listItems.add(empleado.nombre+": \t"+empleado.telefono)

                }
                adapter!!.notifyDataSetChanged()

            }
            .addOnFailureListener { exception ->
                Toast.makeText(
                    activity!!.baseContext, "Error obteniendo los empleados.",
                    Toast.LENGTH_SHORT).show()
            }
    }



}
