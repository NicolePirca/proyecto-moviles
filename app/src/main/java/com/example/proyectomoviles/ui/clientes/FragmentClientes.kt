package com.example.proyectomoviles.ui.clientes

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListView
import android.widget.Toast
import com.example.proyectomoviles.models.Usuario
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore
import android.widget.ArrayAdapter
import com.example.proyectomoviles.NuevoCliente
import com.example.proyectomoviles.R


class FragmentClientes : Fragment() {

    companion object {
        fun newInstance() = FragmentClientes()
    }

    private lateinit var viewModel: FragmentClientesViewModel
    private lateinit var listView:ListView
    private lateinit var nuevoClienteButton:FloatingActionButton
    private val listaClientes = arrayListOf<Usuario>()
    val db = FirebaseFirestore.getInstance()
    var listItems = ArrayList<String>()
    var adapter:ArrayAdapter<String>? = null
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?

    ): View? {
        val viewFragmentClientes =  inflater.inflate(R.layout.fragment_clientes_fragment, container, false)
        //FUNCIONALIDAD ON CREATE VIEW
        listView = viewFragmentClientes.findViewById(R.id.listClientes)
        adapter = ArrayAdapter(activity!!.baseContext, android.R.layout.simple_list_item_1, listItems)
        listView.adapter=adapter
        nuevoClienteButton = viewFragmentClientes.findViewById(R.id.nuevoCLienteButton)
        nuevoClienteButton.setOnClickListener { openNuevoClienteView() }
        leerFirestore()
        return viewFragmentClientes
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FragmentClientesViewModel::class.java)
        // TODO: Use the ViewModel
    }

    fun openNuevoClienteView(){
        val newClienteActivityIntent = Intent (activity, NuevoCliente::class.java)
        startActivity(newClienteActivityIntent)


    }


    private fun leerFirestore(){
        db.collection("users")
            .get()
            .addOnSuccessListener { result ->

                listItems.add("NOMBRE: CORREO")
                for (document in result) {
                    val usuario:Usuario = Usuario(document.data)
                    listaClientes.add(usuario)
                    listItems.add(usuario.nombre+": \t"+usuario.email)

                }
                adapter!!.notifyDataSetChanged()

            }
            .addOnFailureListener { exception ->
                Toast.makeText(
                    activity!!.baseContext, "Error obteniendo los clientes.",
                    Toast.LENGTH_SHORT).show()
            }
    }

}
