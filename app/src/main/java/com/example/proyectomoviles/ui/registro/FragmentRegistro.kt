package com.example.proyectomoviles.ui.registro

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.ListView
import android.widget.Toast
import android.util.Log

import com.example.proyectomoviles.R
import com.example.proyectomoviles.models.Cita
import com.example.proyectomoviles.models.Empleado
import com.example.proyectomoviles.ui.empleados.FragmentEmpleadoViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore

class FragmentRegistro : Fragment() {

    private lateinit var viewModel: FragmentRegistroViewModel
    private lateinit var listView: ListView
    private lateinit var nombreServicio: String
    private lateinit var nombreUsuarioS: String
    private lateinit var nombreEmpleado: String
    private val listaRegistros = arrayListOf<Cita>()
    val db = FirebaseFirestore.getInstance()
    var listItems = ArrayList<String>()
    var adapter: ArrayAdapter<String>? = null


    companion object {
        fun newInstance() = FragmentRegistro()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewFragmentRegistro =
            inflater.inflate(R.layout.fragment_registro_fragment, container, false)
        listView = viewFragmentRegistro.findViewById(R.id.listRegistr)
        adapter =
            ArrayAdapter(activity!!.baseContext, android.R.layout.simple_list_item_1, listItems)
        listView.adapter = adapter
        leerFirestore()
        return viewFragmentRegistro
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FragmentRegistroViewModel::class.java)
        // TODO: Use the ViewModel
    }

    private fun leerFirestore() {

        db.collection("citas")
            .get()
            .addOnSuccessListener { result ->
                listItems.add("CITAS: ")
                for (document in result) {
                    val registro: Cita = Cita(document.data, document.id)

                    // obtenerNombreUsuario(document.getString("idUsuario").toString())
                    listaRegistros.add(registro)
                    //obtenerNombreUsuario(registro.idUsuario)
                    val servicio = obtenerNombreServicio(registro.idServicio)
                   // obtenerNombreEmpleado(registro.idEmpleado)
                  //  obtenerNombreEmpleado(registro.idEmpleado)
                   // nombreUsuarioS = "sssssss"
                    listItems.add(

                        "FECHA: " + registro.fecha + ":\n USUARIO: " +
                                registro.idServicio+ ":\n SERVICIO: " +
                                servicio + ":\n EMPLEADO: " +
                                registro.idEmpleado + ":\n HORARIO: " +
                                registro.horario
                    )
                }
                adapter!!.notifyDataSetChanged()

            }
            .addOnFailureListener { exception ->
                Toast.makeText(
                    activity!!.baseContext, "Error obteniendo las citas.",
                    Toast.LENGTH_SHORT
                ).show()
            }
    }


    fun obtenerNombreUsuario(id: String) {

        val nombreUsuario = db.collection("users").document(id)
        nombreUsuario.get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    nombreUsuarioS = document.getString("nombre").toString()
                    Log.d("NombreUsuario", nombreUsuarioS)
                } else {

                }
            }
            .addOnFailureListener { exception ->

            }
    }

    fun obtenerNombreServicio(id: String) {
        val nombreServicioS = db.collection("services").document(id)
        nombreServicioS.get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    nombreServicio = document.getString("nombre").toString()
                    Log.d("NombreServicio", nombreServicio)
                } else {
                }
            }
            .addOnFailureListener { exception ->
            }
    }
    fun obtenerNombreEmpleado(id: String) {
        val nombreEmpleadoS = db.collection("employees").document(id)
        nombreEmpleadoS.get()
            .addOnSuccessListener { document ->
                if (document.exists()) {
                    nombreEmpleado = document.getString("nombre").toString()
                    Log.d("NombreEmpleado", nombreEmpleado)
                } else {
                }
            }
            .addOnFailureListener { exception ->
            }
    }
}





