package com.example.proyectomoviles.ui.services

import android.content.Intent
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.Toast
import com.example.proyectomoviles.NuevoCliente
import com.example.proyectomoviles.NuevoServicio
import com.example.proyectomoviles.R
import com.example.proyectomoviles.models.Servicio
import com.example.proyectomoviles.models.Usuario
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.firestore.FirebaseFirestore


class FragmentServices : Fragment() {

    companion object {
        fun newInstance() = FragmentServices()
    }

    private lateinit var viewModel: FragmentServicesViewModel
    private lateinit var listView:ListView
    private lateinit var nuevoServiceButton:FloatingActionButton
    private val listaServicios = arrayListOf<Servicio>()
    val db = FirebaseFirestore.getInstance()
    var listItems = ArrayList<String>()
    var adapter:ArrayAdapter<String>? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val viewFragmentServices = inflater.inflate(R.layout.fragment_services_fragment, container,false)
        //FUNCIONALIDAD ON CREATE VIEW
        listView = viewFragmentServices.findViewById(R.id.listService)
        adapter = ArrayAdapter(activity!!.baseContext, android.R.layout.simple_list_item_1, listItems)
        listView.adapter = adapter
        nuevoServiceButton = viewFragmentServices.findViewById(R.id.nuevoServiceButton)
        nuevoServiceButton.setOnClickListener{
            openNuevoServiceView()
        }
        leerFirestore()
        return viewFragmentServices
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(FragmentServicesViewModel::class.java)
        // TODO: Use the ViewModel
    }

    fun openNuevoServiceView(){
        val newServiceActivityIntent = Intent (activity, NuevoServicio::class.java)
        startActivity(newServiceActivityIntent)
    }

    private fun leerFirestore(){
        db.collection("services")
            .get()
            .addOnSuccessListener { result ->

                listItems.add("SERVICIO VALOR")
                for (document in result) {
                    val servicio: Servicio = Servicio(document.data,document.id)
                    listaServicios.add(servicio)
                    listItems.add(servicio.nombre+": \t"+servicio.valor)

                }
                adapter!!.notifyDataSetChanged()

            }
            .addOnFailureListener { exception ->
                Toast.makeText(
                    activity!!.baseContext, "Error obteniendo los clientes.",
                    Toast.LENGTH_SHORT).show()
            }
    }

}
