package com.example.proyectomoviles

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore

class RegisterActivity : AppCompatActivity() {


    private lateinit var registroButton: Button
    private lateinit var backButton: Button
    private lateinit var nombretxt: EditText
    private lateinit var correotxt: EditText
    private lateinit var passwordtxt: EditText
    private lateinit var auth: FirebaseAuth
    val db = FirebaseFirestore.getInstance()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        auth = FirebaseAuth.getInstance()
        registroButton = findViewById(R.id.btnRegistroRegistro)
        backButton = findViewById(R.id.btnReturnLogin)
        nombretxt = findViewById(R.id.txtNombreRegistrar)
        correotxt = findViewById(R.id.txtEmailRegistro)
        passwordtxt = findViewById(R.id.txtPasswordRegistro)
        registroButton.setOnClickListener{registrar()}
        backButton.setOnClickListener{back()}
    }


    private fun back(){
        finish()
    }

    private  fun registrar(){
        registroButton.isEnabled=false
        val nombre = nombretxt.text.toString()
        val email = correotxt.text.toString()
        val password = passwordtxt.text.toString()
        if(email.isNullOrEmpty() || password.isNullOrEmpty() || nombre.isNullOrEmpty()){
            Toast.makeText(baseContext, "Campos vacíos",
                Toast.LENGTH_SHORT).show()
        }else{
            auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Sign in success, update UI with the signed-in user's information

                        val user = auth.currentUser
                        Toast.makeText(baseContext, "Registro Exitoso.",
                            Toast.LENGTH_SHORT).show()

                        val usuario = hashMapOf(
                            "nombre" to nombre,
                            "correo" to email,
                            "tipo" to "cliente"
                        )
                        db.collection("users").document(user!!.uid)
                            .set(usuario as Map<String, Any>)
                            .addOnSuccessListener { documentReference ->

                                registroButton.isEnabled=true
                                val mainActivityIntent = Intent (this, MainActivity::class.java)
                                startActivity(mainActivityIntent)
                            }
                            .addOnFailureListener { e ->
                                Toast.makeText(baseContext, "Registro Fallido en Firestore.",
                                    Toast.LENGTH_SHORT).show()
                                registroButton.isEnabled=true
                            }

                    } else {
                        // If sign in fails, display a message to the user.

                        Toast.makeText(baseContext, "Error en el Registro.",
                            Toast.LENGTH_SHORT).show()
                        registroButton.isEnabled=true

                    }

                    // ...
                }
        }


    }
}
